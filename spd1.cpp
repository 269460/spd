#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>

using namespace std;

// przechowuje informacje o zadaniu
struct Zad {
    int nr; //numer zadania
    int r;  //czas przygotowania
    int p;  //czas trwania 
    int q;  //czas stygnięcia
};

//Wyświetla kolejność wykonywania zadań
void wyswietl_zadania(const vector<Zad>& zadania) {
    cout << "Kolejność uszeregowania:" << endl;
    for (const Zad& zad : zadania) {
        cout << zad.nr << " ";
    }
    cout << endl << endl;
}

//Oblicza wartość Cmax (czas zakończenia ostatniego zadania)
int policzCmax(const vector<Zad>& zadania) {
    int pom1 = 0; // czas zakończenia poprzedniego zadania
    int pom2 = 0; // przechowuje wartość Cmax
    for (const Zad& zad : zadania) {
        pom1 = max(pom1, zad.r) + zad.p; // jest aktualizowane do maksimum z aktualnej wartości pom1 i czasu rozpoczęcia z dodanym czasem trwania zadania 
        pom2 = max(pom2, pom1 + zad.q); //  jest aktualizowane do maksimum z aktualnej wartości pom2 i czasu zakończenia zadania (suma pom1 i czasu stygnięcia )
    }
    return pom2;
}

//Zamienia dwa zadania, zamieniając ich właściwości 
void swapzadania(Zad& a, Zad& b) {
    swap(a.nr, b.nr);
    swap(a.r, b.r);
    swap(a.p, b.p);
    swap(a.q, b.q);
}

//Sortuje wektor zadań w celu minimalizacji wartości Cmax
void permutacjaSort(vector<Zad>& zadania) {
    int dlugosc = policzCmax(zadania);
    int dlugosc2;
    int pom = zadania.size();
    for (int k = 0; k < 10; k++) {
        // Dla każdej pary zadań (o indeksach i i j) 
        for (int i = 0; i < pom; i++) {
            for (int j = i + 1; j < pom; j++) {
                swapzadania(zadania[i], zadania[j]); // Zadania są zamieniane miejscami 
                dlugosc2 = policzCmax(zadania); // obliczana jest nowa wartość Cmax 
                /*jeśli nowa wartość Cmax jest większa niż poprzednia, zamiana zadań pogorszyła wynik,
                 więc następuje ponowne zamienienie zadań miejscami, przywracając poprzednią konfigurację*/
                if (dlugosc2 > dlugosc) {
                    swapzadania(zadania[i], zadania[j]);
                } else {
                    dlugosc = dlugosc2;
                }
            }
        }
    }
}


int main() {
    string nazwa_pliku;
    string tab_nazwy_plikow[4] = { "data.1","data.2","data.3","data.4" };
    int cala_dlugosc = 0;
    vector<int> tab_dlugosci;
    
    chrono::duration<double> czas_uplyniety;

    for (int i = 0; i < 4; i++) {
        cout << "Instancja nr: " << i + 1 << endl;
        cout << "Nazwa pliku: ";
        cin >> nazwa_pliku;
        ifstream input(nazwa_pliku);
        if (!input.is_open()) {
            cout << "Błąd otwarcia pliku!" << endl;
            return 1;
        }

        auto start = chrono::steady_clock::now();
        
        int pom;
        input >> pom;
        vector<Zad> zadania(pom); //wektor zadań na podstawie danych z pliku
        for (int j = 0; j < pom; j++) {
            zadania[j].nr = j + 1;
            input >> zadania[j].r >> zadania[j].p >> zadania[j].q;
        }
        input.close();
        
        permutacjaSort(zadania);
        int dlugosc = policzCmax(zadania);
        tab_dlugosci.push_back(dlugosc);
        cala_dlugosc += dlugosc;
        wyswietl_zadania(zadania);

        auto end = chrono::steady_clock::now();

        czas_uplyniety = czas_uplyniety + (end - start);
    }
    
    for (int i = 0; i < 4; i++) {
        cout << endl << "Dlugość uszeregowań " << i + 1 << ". instancji: " << tab_dlugosci[i] << endl;
    }
    cout << endl << "Suma długości uszeregowań: " << cala_dlugosc << endl << endl;

    cout << "Suma czasów działania dla wszystkich instancji: " << czas_uplyniety.count() << "s\n";
    return 0;
}