

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>

using namespace std;

// Struktura Task reprezentuje zadanie, które zawiera indeks, czas trwania, karę za opóźnienie oraz deadline.
struct Task {
    int index; // Indeks zadania
    int duration; // Czas trwania zadania
    int penalty; // Kara za opóźnienie
    int deadline; // Deadline zadania

    // Operator < przeciążony, aby umożliwić sortowanie zadań według deadline'u.
    bool operator<(const Task& other) const {
        return deadline < other.deadline;
    }
};

// Funkcja porównująca zadania na podstawie ich deadline'u.
bool compareTasks(const Task& a, const Task& b) {
    return a.deadline < b.deadline;
}

// Funkcja drukująca optymalny harmonogram, na podstawie permutacji zadań.
void printOptimalSchedule(const vector<Task>& tasks, const vector<int>& permutation) {
    for (int i = 0; i < permutation.size(); ++i) {
        cout << tasks[permutation[i]].index << " "; // Drukowanie indeksów zadań
    }
}

// Funkcja obliczająca karę za niewykonanie zadań na czas.
int calculatePenalty(const vector<Task>& tasks, const vector<int>& permutation) {
    int total_penalty = 0;
    int current_time = 0;
    for (int i = 0; i < permutation.size(); ++i) {
        int task_idx = permutation[i];
        current_time += tasks[task_idx].duration;
        if (current_time > tasks[task_idx].deadline) {
            total_penalty += tasks[task_idx].penalty * (current_time - tasks[task_idx].deadline);
        }
    }
    return total_penalty;
}

int main(int argc, char* argv[]) {

    chrono::duration<double> czas_uplyniety;
    // Sprawdzenie poprawności argumentów wiersza poleceń.
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " input_file" << endl;
        return 1;
    }

    // Otwarcie pliku wejściowego.
    ifstream input_file(argv[1]);
    if (!input_file) {
        cout << "Error: Unable to open input file." << endl;
        return 1;
    }
auto start = chrono::steady_clock::now();

    int n;
    input_file >> n; // Odczytanie liczby zadań.

    vector<Task> tasks(n); // Wektor przechowujący zadania.

    // Odczytanie danych z pliku i zapisanie ich do wektora zadań.
    for (int i = 0; i < n; ++i) {
        tasks[i].index = i + 1;
        input_file >> tasks[i].duration >> tasks[i].penalty >> tasks[i].deadline;
    }

    input_file.close(); // Zamknięcie pliku wejściowego.

    sort(tasks.begin(), tasks.end(), compareTasks); // Posortowanie zadań według deadline'u.

    vector<int> permutation(n); // Wektor przechowujący permutacje zadań.
    for (int i = 0; i < n; ++i) {
        permutation[i] = i; // Inicjalizacja permutacji.
    }

    int min_penalty = calculatePenalty(tasks, permutation); // Obliczenie kary dla pierwszej permutacji.
    bool first_permutation = true;

    // Próba wszystkich permutacji i znalezienie optymalnej.
    do {
        int penalty = calculatePenalty(tasks, permutation); // Obliczenie kary dla kolejnej permutacji.
        if (penalty < min_penalty) {
            min_penalty = penalty; // Aktualizacja optymalnej kary.
            if (first_permutation) {
                cout << "Optimal schedule: ";
                printOptimalSchedule(tasks, permutation); // Drukowanie optymalnego harmonogramu.
                cout << endl;
                first_permutation = false;
            }
        }
    } while (next_permutation(permutation.begin(), permutation.end()));

auto end = chrono::steady_clock::now();
czas_uplyniety = czas_uplyniety + (end - start);
    // Drukowanie wyniku.
    if (first_permutation) {
        cout << "No valid permutation found." << endl;
    }
    else {
        cout << "Optimal penalty: " << min_penalty << endl;
    }
    cout << "Suma czasów działania dla wszystkich instancji: " << czas_uplyniety.count() << "s\n";
    return 0;
}


