

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <bitset>
#include <cmath>
#include <map>
#include <limits>
#include <chrono>

#define ROZMIAR_BITS 10

using namespace std;

// Struktura Tab reprezentująca informacje o najlepszym dotąd znalezionym harmonogramie

struct Task {
    int index;
    int duration;
    int penalty;
    int deadline;
};

struct Tab {
    int wartosc;        // Wartość minimalnej kary dla danego stanu bitowego
    int index_nizej;    // Indeks stanu bitowego dla najlepszego poprzedniego stanu
    int ktory_task;     // Indeks zadania dodanego w bieżącym kroku
};

// Funkcja porównująca zadania względem terminów wykonania
bool compareTasks(const Task& a, const Task& b) {
    return a.deadline < b.deadline;
}

// Funkcja obliczająca minimalną karę za przekroczenie terminów dla harmonogramu z danym zestawem zadań
int calculate_penalty(vector<Task>& tasks, bitset<ROZMIAR_BITS>& bits, map<int, Tab>& tab, int liczba_zadan) {
    int ctime = 0;                      // Aktualny czas
    int minimalna_wartosc = numeric_limits<int>::max(); // Minimalna wartość kary
    int pom = 0;                        // Pomocnicza zmienna iteracyjna
    int nr_zad = 0;                     // Numer zadania
    int do_kolejnosci = 0;              // Stan bitowy dla najlepszej kolejności zadań
    int jaki_nr_taska = 0;              // Numer zadania do dodania w bieżącym kroku
    bitset<ROZMIAR_BITS> bits_pom(1);   // Pomocniczy bitset

    // Pętla główna, która generuje wszystkie możliwe permutacje zadań
    while (bits.to_ulong() <= pow(2, liczba_zadan) - 1) {
        minimalna_wartosc = numeric_limits<int>::max();

        // Jeśli mamy więcej niż jedno zadanie w harmonogramie
        if (bits.count() != 1) {
            // Obliczamy aktualny czas
            while (pom != liczba_zadan) {
                if (bits[pom] == 1) {
                    ctime += tasks[pom].duration;
                }
                ++pom;
            }
            pom = 0;

            // Dla każdego zadania w harmonogramie
            for (int i = 0; i < liczba_zadan; ++i) {
                if (bits[i] == 1) {
                    bits_pom = bits;
                    bits_pom.reset(i);

                    // Obliczamy aktualną wartość kary dla nowego harmonogramu
                    int aktualna_wartosc = max(ctime - tasks[i].deadline, 0) * tasks[i].penalty + tab[bits_pom.to_ulong()].wartosc;

                    // Jeśli aktualna wartość kary jest mniejsza od minimalnej, aktualizujemy
                    if (aktualna_wartosc < minimalna_wartosc) {
                        minimalna_wartosc = aktualna_wartosc;
                        do_kolejnosci = bits_pom.to_ulong();
                        jaki_nr_taska = i;
                    }
                }
            }
            // Zapisujemy informacje o najlepszym dotąd harmonogramie
            tab[bits.to_ulong()] = {minimalna_wartosc, do_kolejnosci, jaki_nr_taska};
        } else { // Jeśli mamy tylko jedno zadanie w harmonogramie
            // Obliczamy minimalną karę dla jednego zadania
            minimalna_wartosc = max(tasks[bits.to_ulong() - 1].duration - tasks[bits.to_ulong() - 1].deadline, 0) * tasks[bits.to_ulong() - 1].penalty;
            // Zapisujemy informacje o jednym zadaniu
            tab[bits.to_ulong()] = {minimalna_wartosc, -1, bits.to_ulong() - 1};
        }

        // Resetujemy czas i zmienną pomocniczą
        ctime = 0;
        pom = 0;

        // Jeśli wszystkie zadania zostały umieszczone w harmonogramie, przerywamy pętlę
        if (bits.count() == liczba_zadan) {
            break;
        }

        // Przechodzimy do kolejnej permutacji zadań
        bits = bitset<ROZMIAR_BITS>(bits.to_ulong() + 1);
    }

    // Zwracamy minimalną karę dla najlepszego harmonogramu
    return tab[pow(2, liczba_zadan) - 1].wartosc;
}

// Funkcja wyświetlająca optymalną kolejność zadań w harmonogramie
void printOptimalPermutation(vector<Task>& tasks, map<int, Tab>& tab, int idx) {
    if (idx == -1) {
        return;
    }
    printOptimalPermutation(tasks, tab, tab[idx].index_nizej);
    cout << tasks[tab[idx].ktory_task].index << " ";
}


int main(int argc, char* argv[]) {
     chrono::duration<double> czas_uplyniety;
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " input_file" << endl;
        return 1;
    }

    ifstream input_file(argv[1]);
    if (!input_file) {
        cout << "Error: Unable to open input file." << endl;
        return 1;
    }

    int n;
    input_file >> n;

    if (n != ROZMIAR_BITS) {
        cout << "Error: Inconsistent file size. Please change the defined size to match the size in the file." << endl;
        return 2;
    }
auto start = chrono::steady_clock::now();
    vector<Task> tasks(n);
    for (int i = 0; i < n; ++i) {
        tasks[i].index = i + 1;
        input_file >> tasks[i].duration >> tasks[i].penalty >> tasks[i].deadline;
    }

    input_file.close();

    sort(tasks.begin(), tasks.end(), compareTasks);

    map<int, Tab> tab;
    bitset<ROZMIAR_BITS> bits(1);

    int penalty = calculate_penalty(tasks, bits, tab, n);


auto end = chrono::steady_clock::now();
czas_uplyniety = czas_uplyniety + (end - start);

    cout << "\nOptimal schedule: ";
    printOptimalPermutation(tasks, tab, pow(2, n) - 1);
    cout << "\nMinimal penalty: " << penalty << endl;
cout << "Suma czasów działania dla wszystkich instancji: " << czas_uplyniety.count() << "s\n";
    return 0;
}
