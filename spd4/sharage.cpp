#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <algorithm>
#include <chrono> // Include for the chrono library

#define NAZWA_PLIKU "data.008.txt"
#define LICZBA_ZADAN 50

using namespace std;
using namespace std::chrono; // Use the namespace for easy access to chrono functions

struct Zad {
    int r;      // release time
    int p;      // processing time
    int q;      // cooling time
    int nr_zad; // task number
};

vector<Zad> tasks(LICZBA_ZADAN);

bool compareByR(const Zad& a, const Zad& b) {
    return a.r < b.r;
}

bool compareByQ(const Zad& a, const Zad& b) {
    return a.q > b.q;
}

void heapify(vector<Zad>& heap, int i, int n, bool (*compare)(const Zad&, const Zad&)) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && compare(heap[left], heap[largest])) {
        largest = left;
    }

    if (right < n && compare(heap[right], heap[largest])) {
        largest = right;
    }

    if (largest != i) {
        swap(heap[i], heap[largest]);
        heapify(heap, largest, n, compare);
    }
}

void buildHeap(vector<Zad>& heap, bool (*compare)(const Zad&, const Zad&)) {
    for (int i = heap.size() / 2 - 1; i >= 0; i--) {
        heapify(heap, i, heap.size(), compare);
    }
}

Zad extractTop(vector<Zad>& heap, bool (*compare)(const Zad&, const Zad&)) {
    Zad top = heap[0];
    heap[0] = heap.back();
    heap.pop_back();
    heapify(heap, 0, heap.size(), compare);
    return top;
}

void insertHeap(vector<Zad>& heap, Zad zad, bool (*compare)(const Zad&, const Zad&)) {
    heap.push_back(zad);
    int i = heap.size() - 1;
    while (i != 0 && compare(heap[i], heap[(i - 1) / 2])) {
        swap(heap[i], heap[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

int loadTasks() {
    ifstream file(NAZWA_PLIKU);
    if (!file.is_open()) {
        cerr << "Cannot open file!" << endl;
        return -1;
    }

    int numTasks = 0;
    file >> numTasks;
    for (int i = 0; i < numTasks; ++i) {
        file >> tasks[i].r >> tasks[i].p >> tasks[i].q;
        tasks[i].nr_zad = i + 1;
    }
    file.close();
    return numTasks;
}

void schrageAlgorithm(int numTasks) {
    auto start = high_resolution_clock::now(); // Start timing here

    vector<Zad> N = tasks; // Unready tasks
    vector<Zad> G;         // Ready tasks
    buildHeap(N, compareByR); // Build a min-heap based on r

    int t = 0;  // current time
    int Cmax = 0;
    vector<int> taskOrder;

    while (!G.empty() || !N.empty()) {
        while (!N.empty() && N[0].r <= t) {
            insertHeap(G, extractTop(N, compareByR), compareByQ); // Move to G, max-heap by q
        }
        if (G.empty()) {
            t = N[0].r;
        } else {
            Zad current = extractTop(G, compareByQ);
            taskOrder.push_back(current.nr_zad);
            t += current.p;
            Cmax = max(Cmax, t + current.q);
        }
    }

    auto stop = std::chrono::high_resolution_clock::now(); // Zakończenie pomiaru czasu
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); // Przeliczenie na mikrosekundy

    cout << "Cmax is: " << Cmax << endl;
    cout << "Elapsed time: " << duration.count() << " us" << endl; 
    for (int id : taskOrder) {
        cout << id << " ";
    }
    cout << endl;
}

int main() {
    int numTasks = loadTasks();
    cout << "Tasks loaded: " << numTasks << endl;

    schrageAlgorithm(numTasks);

    return 0;
}
