#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <algorithm>
#include <chrono>  // Include for high-resolution timing

#define NAZWA_PLIKU "data.008.txt"
#define LICZBA_ZADAN 50

using namespace std;

struct Zad {
    int r;      // release time
    int p;      // processing time
    int q;      // cooling time
    int nr_zad; // task number
    int original_p; // original processing time for restoring after preemption
};

vector<Zad> tasks(LICZBA_ZADAN);

bool compareByR(const Zad& a, const Zad& b) {
    return a.r < b.r;
}

bool compareByQ(const Zad& a, const Zad& b) {
    return a.q > b.q;
}

void heapify(vector<Zad>& heap, int i, int n, bool (*compare)(const Zad&, const Zad&)) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && compare(heap[left], heap[largest])) {
        largest = left;
    }

    if (right < n && compare(heap[right], heap[largest])) {
        largest = right;
    }

    if (largest != i) {
        swap(heap[i], heap[largest]);
        heapify(heap, largest, n, compare);
    }
}

void buildHeap(vector<Zad>& heap, bool (*compare)(const Zad&, const Zad&)) {
    for (int i = heap.size() / 2 - 1; i >= 0; i--) {
        heapify(heap, i, heap.size(), compare);
    }
}

Zad extractTop(vector<Zad>& heap, bool (*compare)(const Zad&, const Zad&)) {
    Zad top = heap[0];
    heap[0] = heap.back();
    heap.pop_back();
    heapify(heap, 0, heap.size(), compare);
    return top;
}

void insertHeap(vector<Zad>& heap, Zad zad, bool (*compare)(const Zad&, const Zad&)) {
    heap.push_back(zad);
    int i = heap.size() - 1;
    while (i != 0 && compare(heap[i], heap[(i - 1) / 2])) {
        swap(heap[i], heap[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

int loadTasks() {
    ifstream file(NAZWA_PLIKU);
    if (!file.is_open()) {
        cerr << "Cannot open file!" << endl;
        return -1;
    }

    int numTasks = 0;
    file >> numTasks;
    for (int i = 0; i < numTasks; ++i) {
        file >> tasks[i].r >> tasks[i].p >> tasks[i].q;
        tasks[i].nr_zad = i + 1;
        tasks[i].original_p = tasks[i].p;
    }
    file.close();
    return numTasks;
}

void schrageAlgorithmWithPreemption(int numTasks) {
    auto start = chrono::high_resolution_clock::now();  // Start timing

    vector<Zad> N = tasks; // Unready tasks
    vector<Zad> G;         // Ready tasks
    buildHeap(N, compareByR); // Build a min-heap based on r

    int t = 0;  // current time
    int Cmax = 0;
    vector<int> taskOrder;
    Zad l = {0, 0, INT_MAX, 0, 0}; // Current task with infinite cooling time

    while (!G.empty() || !N.empty()) {
        while (!N.empty() && N[0].r <= t) {
            Zad e = extractTop(N, compareByR);
            insertHeap(G, e, compareByQ); // Move to G, max-heap by q
            if (e.q > l.q) {
                if (l.nr_zad != 0) {
                    l.p = t - e.r;
                    if (l.p > 0) {
                        insertHeap(G, l, compareByQ);
                    }
                }
                t = e.r;
            }
        }

        if (G.empty()) {
            t = N[0].r;
        } else {
            Zad e = extractTop(G, compareByQ);
            l = e;
            taskOrder.push_back(e.nr_zad);
            t += e.p;
            Cmax = max(Cmax, t + e.q);
        }
    }

    auto end = chrono::high_resolution_clock::now();  // Stop timing
    chrono::duration<double, milli> elapsed = end - start;  // Calculate elapsed time

    cout << "Cmax is: " << Cmax << endl;
    cout << "Elapsed time: " << elapsed.count() << " ms" << endl;
    for (int id : taskOrder) {
        cout << id << " ";
    }
    cout << endl;
}

int main() {
    int numTasks = loadTasks();
    cout << "Tasks loaded: " << numTasks << endl;

    schrageAlgorithmWithPreemption(numTasks);

    return 0;
}
